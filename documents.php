<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <div class="heading">
                <div class="container">
                    <h1><span>Документы</span></h1>
                    <ul class="breadcrumb">
                        <li><a href="#">Главная</a></li>
                        <li>Документы</li>
                    </ul>
                </div>
            </div>

            <div class="main">
                <div class="subnav">
                    <div class="container">
                        <ul>
                            <li><a href="#">компания</a></li>
                            <li class="active"><a href="#">дилеры</a></li>
                            <li><a href="#">партнеры</a></li>
                            <li><a href="#">Сервис</a></li>
                        </ul>
                    </div>
                </div>
                <div class="content-gray content">
                    <div class="container">
                        <h2 class="text-center"><span>Документы</span></h2>
                        <ul class="docs">
                            <li>
                                <div class="docs__image">
                                    <a href="images/doc.jpg" data-fancybox="docs">
                                        <img src="images/doc.jpg" class="img-fluid" alt="">
                                        <i class="fa fa-search-plus"></i>
                                    </a>
                                </div>
                                <div class="docs__text">
                                <span>
                                    Свидетельство Саморегулируемойорганизации Ассоциации строителей"Межрегионстройальянс"
                                    <br/>СРО-С-253-05092012 от 15 июня 2017 г. № 0840.01-2017-5010019225-С-253
                                </span>
                                </div>
                            </li>
                            <li>
                                <a class="docs__image" href="images/doc.jpg" data-fancybox="docs">
                                    <img src="images/doc.jpg" class="img-fluid" alt="">
                                </a>
                                <div class="docs__text">
                                <span>
                                    Свидетельство Саморегулируемойорганизации Ассоциации строителей"Межрегионстройальянс"
                                    <br/>СРО-С-253-05092012 от 15 июня 2017 г. № 0840.01-2017-5010019225-С-253
                                </span>
                                </div>
                            </li>
                            <li>
                                <a class="docs__image" href="images/doc.jpg" data-fancybox="docs">
                                    <img src="images/doc.jpg" class="img-fluid" alt="">
                                </a>
                                <div class="docs__text">
                                <span>
                                    Свидетельство Саморегулируемойорганизации Ассоциации строителей"Межрегионстройальянс"
                                    <br/>СРО-С-253-05092012 от 15 июня 2017 г. № 0840.01-2017-5010019225-С-253
                                </span>
                                </div>
                            </li>
                            <li>
                                <a class="docs__image" href="images/doc.jpg" data-fancybox="docs">
                                    <img src="images/doc.jpg" class="img-fluid" alt="">
                                </a>
                                <div class="docs__text">
                                <span>
                                    Свидетельство Саморегулируемойорганизации Ассоциации строителей"Межрегионстройальянс"
                                    <br/>СРО-С-253-05092012 от 15 июня 2017 г. № 0840.01-2017-5010019225-С-253
                                </span>
                                </div>
                            </li>
                            <li>
                                <a class="docs__image" href="images/doc.jpg" data-fancybox="docs">
                                    <img src="images/doc.jpg" class="img-fluid" alt="">
                                </a>
                                <div class="docs__text">
                                <span>
                                    Свидетельство Саморегулируемойорганизации Ассоциации строителей"Межрегионстройальянс"
                                    <br/>СРО-С-253-05092012 от 15 июня 2017 г. № 0840.01-2017-5010019225-С-253
                                </span>
                                </div>
                            </li>
                            <li>
                                <a class="docs__image" href="images/doc.jpg" data-fancybox="docs">
                                    <img src="images/doc.jpg" class="img-fluid" alt="">
                                </a>
                                <div class="docs__text">
                                <span>
                                    Свидетельство Саморегулируемойорганизации Ассоциации строителей"Межрегионстройальянс"
                                    <br/>СРО-С-253-05092012 от 15 июня 2017 г. № 0840.01-2017-5010019225-С-253
                                </span>
                                </div>
                            </li>
                            <li>
                                <a class="docs__image" href="images/doc.jpg" data-fancybox="docs">
                                    <img src="images/doc.jpg" class="img-fluid" alt="">
                                </a>
                                <div class="docs__text">
                                <span>
                                    Свидетельство Саморегулируемойорганизации Ассоциации строителей"Межрегионстройальянс"
                                    <br/>СРО-С-253-05092012 от 15 июня 2017 г. № 0840.01-2017-5010019225-С-253
                                </span>
                                </div>
                            </li>
                            <li>
                                <a class="docs__image" href="images/doc.jpg" data-fancybox="docs">
                                    <img src="images/doc.jpg" class="img-fluid" alt="">
                                </a>
                                <div class="docs__text">
                                <span>
                                    Свидетельство Саморегулируемойорганизации Ассоциации строителей"Межрегионстройальянс"
                                    <br/>СРО-С-253-05092012 от 15 июня 2017 г. № 0840.01-2017-5010019225-С-253
                                </span>
                                </div>
                            </li>
                        </ul>
                        <ul class="pagination">
                            <li><a href="#"><i class="fa fa-angle-left"></i></a></li>
                            <li class="active"><a href="#">01</a></li>
                            <li><a href="#">02</a></li>
                            <li><a href="#">03</a></li>
                            <li><a href="#">04</a></li>
                            <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Callback -->
        <?php include('inc/callback.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
