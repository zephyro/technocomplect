<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <div class="heading">
                <div class="container">
                    <h1><span>Дилеры</span></h1>
                    <ul class="breadcrumb">
                        <li><a href="#">Главная</a></li>
                        <li>Дилеры</li>
                    </ul>
                </div>
            </div>

            <div class="main">
                <div class="subnav">
                    <div class="container">
                        <ul>
                            <li><a href="#">компания</a></li>
                            <li class="active"><a href="#">дилеры</a></li>
                            <li><a href="#">партнеры</a></li>
                            <li><a href="#">Сервис</a></li>
                        </ul>
                    </div>
                </div>
                <div class="content-gray content-dealers">
                    <h2 class="text-center"><span>Дилеры</span></h2>
                    <div class="dealers">
                        <div class="dealers__contact">
                            <div class="scroll-wrap">
                                <div class="dealers__list">
                                    <div class="contactBox">
                                        <div class="contactBox__heading"><a href="#">ЗАО "Техноэнергокомплект"</a></div>
                                        <div class="contactBox__address">443031, г. Самара, проспект Кирова, 435 Тел./факс: +7 (846) 200-15-90, 200-15-44, 200-16-20</div>
                                        <ul>
                                            <li><a href="#">Дилерский сертификат</a></li>
                                            <li><strong>E-mail:</strong> <a href="#">info@thc-samara.ru</a></li>
                                            <li><strong>Web:</strong> <a href="#">http://www.thc-samara.ru</a></li>
                                        </ul>
                                    </div>
                                    <div class="contactBox">
                                        <div class="contactBox__heading"><a href="#">ООО "ТЕХНОКОМПЛЕКТ-Энерго"</a></div>
                                        <div class="contactBox__address">Республика Беларусь, г. Гомель, ул. Федосеенко, д.8, к.2</div>
                                    </div>
                                    <div class="contactBox">
                                        <div class="contactBox__heading"><a href="#">ООО «ЭлТехКом-Энергосбережение»</a></div>
                                        <div class="contactBox__address">460052, г. Оренбург, ул. Салмышская, д.39</div>
                                    </div>
                                    <div class="contactBox">
                                        <div class="contactBox__heading"><a href="#">ЗАО "Техноэнергокомплект"</a></div>
                                        <div class="contactBox__address">443031, г. Самара, проспект Кирова, 435 Тел./факс: +7 (846) 200-15-90, 200-15-44, 200-16-20</div>
                                        <ul>
                                            <li><a href="#">Дилерский сертификат</a></li>
                                            <li><strong>E-mail:</strong> <a href="#">info@thc-samara.ru</a></li>
                                            <li><strong>Web:</strong> <a href="#">http://www.thc-samara.ru</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="dealers__map">
                            <div id="map"></div>
                            <div class="dealers__wrap visible">
                                <div class="dealers__box">
                                    <span class="dealers__box_close"></span>
                                    <div class="dealers__box_heading"><a href="#">ЗАО "Техноэнергокомплект"</a></div>
                                    <div class="dealers__box_address">443031, г. Самара, проспект Кирова, 435 Тел./факс: +7 (846) 200-15-90, 200-15-44, 200-16-20</div>
                                    <ul>
                                        <li><a href="#">Дилерский сертификат</a></li>
                                        <li><strong>E-mail:</strong> <a href="#">info@thc-samara.ru</a></li>
                                        <li><strong>Web:</strong> <a href="#">http://www.thc-samara.ru</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Callback -->
        <?php include('inc/callback.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

        <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>

        <script>
            var myMap;

            ymaps.ready(function () {
                myMap = new ymaps.Map('map', {
                    zoom: 15,
                    center: [55.7627216492089,37.621767357191366],
                    controls: ['smallMapDefaultSet']
                }, {
                    searchControlProvider: 'yandex#search'
                });
                myMap.geoObjects
                    .add(new ymaps.Placemark([55.75977785652087,37.61999014813234], {
                        balloonContent: ''
                    }, {
                        preset: 'islands#icon',
                        iconColor: '#99afd'
                    }));
                myMap.geoObjects
                    .add(new ymaps.Placemark([55.763229774483264,37.61794789155416], {
                        balloonContent: ''
                    }, {
                        preset: 'islands#icon',
                        iconColor: '#99afd'
                    }));
                myMap.geoObjects
                    .add(new ymaps.Placemark([55.76214092646699,37.625372246107375], {
                        balloonContent: ''
                    }, {
                        preset: 'islands#icon',
                        iconColor: '#99afd'
                    }));
            });
        </script>

    </body>
</html>
