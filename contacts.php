<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <div class="heading">
                <div class="container">
                    <h1><span>контакты</span></h1>
                    <ul class="breadcrumb">
                        <li><a href="#">Главная</a></li>
                        <li>контакты</li>
                    </ul>
                </div>
            </div>

            <div class="main">
                <div class="subnav">
                    <div class="container">
                        <ul>
                            <li class="active"><a href="#">компания</a></li>
                            <li><a href="#">дилеры</a></li>
                            <li><a href="#">партнеры</a></li>
                            <li><a href="#">Сервис</a></li>
                        </ul>
                    </div>
                </div>

                <div class="content-gray content">
                    <div class="container">

                        <h2 class="text-center"><span>Зао «МПОК ТЕХНОКОМПЛЕКТ»</span></h2>

                        <div class="contact-row">

                            <div class="contact-elem">
                                <div class="card">
                                    <div class="card__heading">Администрация</div>
                                    <div class="card__data">
                                        <span>Тел./факс приемной:</span> <strong>(496) 219-88-00/01</strong>
                                        <br/>
                                        <a href="mailto:techno@dubna.ru">techno@dubna.ru</a>
                                    </div>
                                    <a href="#contact_1" class="card__view btn-modal">Все номера</a>
                                </div>

                                <!-- Все контакты -->
                                <div class="hide">
                                    <div class="contactModal" id="contact_1">
                                        <div class="scroller">
                                            <div class="contactModal__wrap">
                                                <div class="modal__heading text-center"><span>коммерческая служба</span></div>
                                                <ul class="contactModal__data">
                                                    <li>
                                                        <h3>Заместитель коммерческого директора</h3>
                                                        Колгин Сергей Анатольевич<br/>
                                                        <strong>Телефон:</strong> (496) 219-88-00/01
                                                    </li>
                                                    <li>
                                                        <h3>Заместитель коммерческого директора</h3>
                                                        Колгин Сергей Анатольевич<br/>
                                                        <strong>Телефон:</strong> (496) 219-88-00/01<br/>
                                                        <strong>E-mail:</strong> <a href="mailto:techno@dubna.ru">techno@dubna.ru</a>
                                                    </li>
                                                    <li>
                                                        <h3>Заместитель коммерческого директора</h3>
                                                        Колгин Сергей Анатольевич<br/>
                                                        <strong>Телефон:</strong> (496) 219-88-00/01
                                                    </li>
                                                    <li>
                                                        <h3>Заместитель коммерческого директора</h3>
                                                        Колгин Сергей Анатольевич<br/>
                                                        <strong>Телефон:</strong> (496) 219-88-00/01
                                                    </li>
                                                    <li>
                                                        <h3>Заместитель коммерческого директора</h3>
                                                        Колгин Сергей Анатольевич<br/>
                                                        <strong>Телефон:</strong> (496) 219-88-00/01
                                                        <strong>E-mail:</strong>     <a href="mailto:techno@dubna.ru">techno@dubna.ru</a>
                                                    </li>
                                                    <li>
                                                        <h3>Заместитель коммерческого директора</h3>
                                                        Колгин Сергей Анатольевич<br/>
                                                        <strong>Телефон:</strong> (496) 219-88-00/01
                                                        <strong>E-mail:</strong>     <a href="mailto:techno@dubna.ru">techno@dubna.ru</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- -->

                            </div>

                            <div class="contact-elem">
                                <div class="card">
                                    <div class="card__heading">Коммерческая служба</div>
                                    <div class="card__data">
                                        <span>Тел./факс:</span> <strong>(496) 219-88-48/90</strong>
                                        <br/>
                                        <a href="mailto:ks@techno-com.ru">ks@techno-com.ru</a>
                                    </div>
                                    <a href="#contact_2" class="card__view btn-modal">Все номера</a>

                                    <!-- Все контакты -->
                                    <div class="hide">
                                        <div class="contactModal" id="contact_2">
                                            <div class="scroller">
                                                <div class="contactModal__wrap">
                                                    <div class="modal__heading text-center"><span>коммерческая служба</span></div>
                                                    <ul class="contactModal__data">
                                                        <li>
                                                            <h3>Заместитель коммерческого директора</h3>
                                                            Колгин Сергей Анатольевич<br/>
                                                            <strong>Телефон:</strong> (496) 219-88-00/01
                                                        </li>
                                                        <li>
                                                            <h3>Заместитель коммерческого директора</h3>
                                                            Колгин Сергей Анатольевич<br/>
                                                            <strong>Телефон:</strong> (496) 219-88-00/01<br/>
                                                            <strong>E-mail:</strong> <a href="mailto:techno@dubna.ru">techno@dubna.ru</a>
                                                        </li>
                                                        <li>
                                                            <h3>Заместитель коммерческого директора</h3>
                                                            Колгин Сергей Анатольевич<br/>
                                                            <strong>Телефон:</strong> (496) 219-88-00/01
                                                        </li>
                                                        <li>
                                                            <h3>Заместитель коммерческого директора</h3>
                                                            Колгин Сергей Анатольевич<br/>
                                                            <strong>Телефон:</strong> (496) 219-88-00/01
                                                        </li>
                                                        <li>
                                                            <h3>Заместитель коммерческого директора</h3>
                                                            Колгин Сергей Анатольевич<br/>
                                                            <strong>Телефон:</strong> (496) 219-88-00/01
                                                            <strong>E-mail:</strong>     <a href="mailto:techno@dubna.ru">techno@dubna.ru</a>
                                                        </li>
                                                        <li>
                                                            <h3>Заместитель коммерческого директора</h3>
                                                            Колгин Сергей Анатольевич<br/>
                                                            <strong>Телефон:</strong> (496) 219-88-00/01
                                                            <strong>E-mail:</strong>     <a href="mailto:techno@dubna.ru">techno@dubna.ru</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- -->
                                </div>
                            </div>

                        </div>

                        <div class="place">
                            <div class="place__content">
                                <div class="place__elem">
                                    <div class="card">
                                        <div class="card__heading">Офис</div>
                                        <div class="card__data">141981, г. Дубна Московской обл., ул. Школьная д.10а<br/>Тел./факс: (496) 219-88-00/0</div>
                                        <a href="#" class="card__view">Показать на карте</a>
                                    </div>
                                </div>
                                <div class="place__elem">
                                    <div class="card">
                                        <div class="card__heading">Производство</div>
                                        <div class="card__data">141980, г.Дубна, ул. Электронная, д.5 (на территории НПЗ "ОЭЗ "Дубна")<br/>Тел./факс: (496) 212-82-32</div>
                                        <a href="#" class="card__view">Показать на карте</a>
                                    </div>
                                </div>
                            </div>
                            <div class="place__map">
                                <div id="map"></div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="content">
                    <div class="container">
                        <div class="second-contact">

                            <div class="second-contact-item">
                                <h3>Отдел снабжения</h3>
                                <p>
                                    <span>Тел./факс:</span>
                                    <strong>(496) 219-88-66/22</strong>
                                </p>
                            </div>

                            <div class="second-contact-item">
                                <h3>Отдел снабжения</h3>
                                <p>
                                    <span>Тел./факс:</span>
                                    <strong>(496) 219-88-66/22</strong>
                                </p>
                            </div>

                            <div class="second-contact-item">
                                <h3>Отдел снабжения</h3>
                                <p>
                                    <span>Тел./факс:</span>
                                    <strong>(496) 219-88-66/22</strong>
                                </p>
                            </div>

                            <div class="second-contact-item">
                                <h3>Отдел снабжения</h3>
                                <p>
                                    <span>Тел./факс:</span>
                                    <strong>(496) 219-88-66/22</strong>
                                </p>
                            </div>

                            <div class="second-contact-item">
                                <h3>Отдел снабжения</h3>
                                <p>
                                    <span>Тел./факс:</span>
                                    <strong>(496) 219-88-66/22</strong>
                                </p>
                            </div>

                        </div>
                    </div>
                </div>

            </div>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Callback -->
        <?php include('inc/callback.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->


        <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>

        <script>
            var myMap;

            ymaps.ready(function () {
                myMap = new ymaps.Map('map', {
                    zoom: 15,
                    center: [55.7627216492089,37.621767357191366],
                    controls: ['smallMapDefaultSet']
                }, {
                    searchControlProvider: 'yandex#search'
                });
                myMap.geoObjects
                    .add(new ymaps.Placemark([55.75977785652087,37.61999014813234], {
                        balloonContent: ''
                    }, {
                        preset: 'islands#icon',
                        iconColor: '#99afd'
                    }));
                myMap.geoObjects
                    .add(new ymaps.Placemark([55.763229774483264,37.61794789155416], {
                        balloonContent: ''
                    }, {
                        preset: 'islands#icon',
                        iconColor: '#99afd'
                    }));
                myMap.geoObjects
                    .add(new ymaps.Placemark([55.76214092646699,37.625372246107375], {
                        balloonContent: ''
                    }, {
                        preset: 'islands#icon',
                        iconColor: '#99afd'
                    }));
            });
        </script>

    </body>
</html>
