<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <div class="heading">
                <div class="container">
                    <h1><span>продукты</span></h1>
                    <ul class="breadcrumb">
                        <li><a href="#">Главная</a></li>
                        <li>продукты</li>
                    </ul>
                </div>
            </div>

            <div class="main">
                <div class="subnav">
                    <div class="container">
                        <ul>
                            <li class="active"><a href="#">системы постоянного тока</a></li>
                            <li><a href="#">Системы переменного тока</a></li>
                            <li><a href="#">другие продукты</a></li>
                        </ul>
                    </div>
                </div>
                <div class="content-gray content">
                    <div class="container">
                        <h2 class="text-center"><span>системы постоянного тока</span></h2>

                        <div class="product-row">
                            <div class="product-elem">
                                <a href="#" class="goods">
                                    <div class="goods__image">
                                        <img src="images/product_01.jpg" class="img-fluid" alt="">
                                    </div>
                                    <h3>АУОТ-М «Дубна»</h3>
                                    <span>Зарядные устройства</span>
                                    <span class="goods__tag special">спецпредложение</span>
                                </a>
                            </div>

                            <div class="product-elem">
                                <a href="#" class="goods">
                                    <div class="goods__image">
                                        <img src="images/product_01.jpg" class="img-fluid" alt="">
                                    </div>
                                    <h3>АУОТ-М «Дубна»</h3>
                                    <span>Зарядные устройства</span>
                                    <span class="goods__tag new">новинки</span>
                                </a>
                            </div>

                            <div class="product-elem">
                                <a href="#" class="goods">
                                    <div class="goods__image">
                                        <img src="images/product_02.jpg" class="img-fluid" alt="">
                                    </div>
                                    <h3>АУОТ-М «Дубна»</h3>
                                    <span>Зарядные устройства</span>
                                </a>
                            </div>

                            <div class="product-elem">
                                <a href="#" class="goods">
                                    <div class="goods__image">
                                        <img src="images/product_04.jpg" class="img-fluid" alt="">
                                    </div>
                                    <h3>Шкаф блока предохранителей</h3>
                                    <span>Контроль-измерительное оборудование</span>
                                </a>
                            </div>

                            <div class="product-elem">
                                <a href="#" class="goods">
                                    <div class="goods__image">
                                        <img src="images/product_05.jpg" class="img-fluid" alt="">
                                    </div>
                                    <h3>ПКИ-07 «Дубна»</h3>
                                    <span>Устройства гарантированного питания</span>
                                </a>
                            </div>

                            <div class="product-elem">
                                <a href="#" class="goods">
                                    <div class="goods__image">
                                        <img src="images/product_01.jpg" class="img-fluid" alt="">
                                    </div>
                                    <h3>АУОТ-М «Дубна»</h3>
                                    <span>Зарядные устройства</span>
                                </a>
                            </div>

                            <div class="product-elem">
                                <a href="#" class="goods">
                                    <div class="goods__image">
                                        <img src="images/product_02.jpg" class="img-fluid" alt="">
                                    </div>
                                    <h3>АУОТ-М «Дубна»</h3>
                                    <span>Зарядные устройства</span>
                                </a>
                            </div>

                            <div class="product-elem">
                                <a href="#" class="goods">
                                    <div class="goods__image">
                                        <img src="images/product_01.jpg" class="img-fluid" alt="">
                                    </div>
                                    <h3>АУОТ-М «Дубна»</h3>
                                    <span>Зарядные устройства</span>
                                </a>
                            </div>

                            <div class="product-elem">
                                <a href="#" class="goods">
                                    <div class="goods__image">
                                        <img src="images/product_01.jpg" class="img-fluid" alt="">
                                    </div>
                                    <h3>АУОТ-М «Дубна»</h3>
                                    <span>Зарядные устройства</span>
                                </a>
                            </div>
                        </div>

                        <div class="text-center">
                            <a href="#" class="btn">Показать еще товары</a>
                        </div>
                    </div>
                </div>

                <div class="content">
                    <div class="container">
                        <div class="text-content">
                            <p>Основанная в 1996 году в г. Дубна ЗАО «МПОТК «ТЕХНОКОМПЛЕКТ» многие годы
                                занимается разработкой, производством и поставкой электротехнического оборудования.</p>
                            <p>Сегодня на предприятии работает более 250 высокопрофессиональных специалистов, решающих самые сложные и нетривиальные задачи.</p>
                            <p>Разработка и широкое использование инновационных технологий в производственной деятельности компании обеспечили ее лидирующие позиции на рынке силовых полупроводниковых технологий, а независимый внутренний производственный цикл предопределил безусловные ценовые и качественные конкурентные преимущества производимой продукции.</p>
                        </div>
                    </div>
                </div>

            </div>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Callback -->
        <?php include('inc/callback.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

        <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>

        <script>
            var myMap;

            ymaps.ready(function () {
                myMap = new ymaps.Map('map', {
                    zoom: 17,
                    center: [55.81541256892136,37.514804999999974],
                    controls: ['smallMapDefaultSet']
                }, {
                    searchControlProvider: 'yandex#search'
                });
                myMap.geoObjects
                    .add(new ymaps.Placemark([55.81541256892136,37.514804999999974], {
                        balloonContent: ''
                    }, {
                        preset: 'islands#icon',
                        iconColor: '#99afd'
                    }));
            });
        </script>

    </body>
</html>
