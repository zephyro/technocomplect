<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <div class="heading">
                <div class="container">
                    <h1><span>Заголовок H1</span></h1>
                    <ul class="breadcrumb">
                        <li><a href="#">Главная</a></li>
                        <li><a href="#">Системы постоянного тока</a></li>
                        <li>АОУТ-М «ДУБНА»</li>
                    </ul>
                </div>
            </div>

            <div class="main">
 
                <div class="content">
                    <div class="container">
                        <div class="text-content">
                            <h2 class="text-center"><span>Заголовок H2</span></h2>


                            <p>Электропитание изделий от основной и резервной сетей трёхфазного переменного тока с действием АВР;</p>
                            <ul>

                                <li>Преобразование напряжения сети переменного тока в напряжение постоянного тока осуществляется при помощи двух взаимно резервируемых независимых преобразователей на базе IGBT-транзисторов, с возможностью объединения мощности;</li>
                                <li>Охлаждение преобразовательных блоков – воздушное, принудительное с пропорциональным управлением вентиляторами;</li>
                                <li>Возможность дистанционного управление внешними сигналами типа «сухой контакт» с действиями:</li>
                                <li>Ускоренный заряд аккумуляторной батареи методами U, IU, IUI;</li>
                                <li>Выравнивающий заряд АБ;</li>
                            </ul>

                            <br/>
                            <br/>

                            <h3>Заголовок H3</h3>

                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                            <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

                            <br/>

                            <div class="imgBlock">
                                <div class="imgBlock__item">
                                    <a href="images/img_01.jpg" data-fancybox="gallery">
                                        <img src="images/img_01.jpg" class="img-fluid" alt="">
                                    </a>
                                </div>
                                <div class="imgBlock__item">
                                    <a href="images/img_02.jpg" data-fancybox="gallery">
                                        <img src="images/img_02.jpg" class="img-fluid" alt="">
                                    </a>
                                </div>
                            </div>

                            <br/>

                            <h4>Заголовок Н4</h4>
                            <div class="image">
                                <img src="images/image.jpg" class="img-fluid">
                            </div>
                            <div class="image-description">Подпись к картинке</div>

                        </div>

                    </div>
                </div>

                <div class="content-gray content">
                    <div class="container">
                        <p class="text-center">Название таблицы</p>

                        <div class="table-responsive">
                            <table class="table">
                                <tr>
                                    <th>Питающая сеть</th>
                                    <th>Номинальный выходной ток при одновременной работе преобразовательных блоков*), [А]</th>
                                    <th>Диапазон регулирования выходного напряжения [B]</th>
                                    <th>Номинальное выходное напряжение [B]</th>
                                </tr>
                                <tr>
                                    <td rowspan="3">3х380 В, <br/>45 ÷ 55 Гц</td>
                                    <td>50 (80*)</td>
                                    <td>85 - 140</td>
                                    <td>115</td>
                                </tr>
                                <tr>
                                    <td>50 (80*)</td>
                                    <td>85 - 140</td>
                                    <td>115</td>
                                </tr>
                                <tr>
                                    <td>50 (80*)</td>
                                    <td>85 - 140</td>
                                    <td>115</td>
                                </tr>

                                <tr>
                                    <td rowspan="3">3х380 В, <br/>45 ÷ 55 Гц</td>
                                    <td>50 (80*)</td>
                                    <td>85 - 140</td>
                                    <td>115</td>
                                </tr>
                                <tr>
                                    <td>50 (80*)</td>
                                    <td>85 - 140</td>
                                    <td>115</td>
                                </tr>
                                <tr>
                                    <td>50 (80*)</td>
                                    <td>85 - 140</td>
                                    <td>115</td>
                                </tr>

                                <tr>
                                    <td colspan="4" class="table-subtitle">Входные параметры</td>
                                </tr>
                                <tr>
                                    <td rowspan="3">3х380 В, <br/>45 ÷ 55 Гц</td>
                                    <td>50 (80*)</td>
                                    <td>85 - 140</td>
                                    <td>115</td>
                                </tr>
                                <tr>
                                    <td>50 (80*)</td>
                                    <td>85 - 140</td>
                                    <td>115</td>
                                </tr>
                                <tr>
                                    <td>50 (80*)</td>
                                    <td>85 - 140</td>
                                    <td>115</td>
                                </tr>

                            </table>
                        </div>

                    </div>
                </div>

            </div>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Callback -->
        <?php include('inc/callback.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
