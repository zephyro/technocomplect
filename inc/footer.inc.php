<footer class="footer">

    <div class="footer__top">
        <div class="container">
            <nav class="footer__nav">
                <div class="footer__nav_item">
                    <h4>каталог</h4>
                    <ul>
                        <li><a href="#">Продукты</a></li>
                        <li><a href="#">Услуги</a></li>
                        <li><a href="#">Наука</a></li>
                        <li><a href="#">Работы</a></li>
                    </ul>
                </div>
                <div class="footer__nav_item">
                    <h4>компания</h4>
                    <ul>
                        <li><a href="#">Документы</a></li>
                        <li><a href="#">Отзывы</a></li>
                        <li><a href="#">Истории</a></li>
                        <li><a href="#">Вакансии</a></li>
                    </ul>
                </div>
                <div class="footer__nav_item">
                    <h4>события</h4>
                    <ul>
                        <li><a href="#">Новости</a></li>
                        <li><a href="#">Публикации</a></li>
                        <li><a href="#">Выставки</a></li>
                        <li><a href="#">Совещания</a></li>
                    </ul>
                </div>
                <div class="footer__nav_item">
                    <h4>контакты</h4>
                    <ul>
                        <li><a href="#">Компания</a></li>
                        <li><a href="#">Диллеры</a></li>
                        <li><a href="#">Партнеры</a></li>
                        <li><a href="#">Сервис</a></li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>

    <div class="footer__bottom">
        <div class="container">
            <div class="footer__row">
                <div class="footer__company">
                    ЗАО «МПОТК «Технокомплект»<br/>
                    © 2003-2018 все права защищены
                </div>
                <div class="footer__privacy"><a class="btn-modal" href="#privacy">Политика конфиденциальности</a></div>
                <ul class="footer__contact">
                    <li><a class="tel" href="tel:+74962198800">+7 (496) 219-88-00</a></li>
                    <li><a href="#callback" class="btn-callback btn-modal">Обратный звонок</a></li>
                </ul>
                <div class="footer__dev">
                    <a href="#"><span>Дизайн и разработка —</span></a>
                </div>
            </div>
        </div>
    </div>

</footer>