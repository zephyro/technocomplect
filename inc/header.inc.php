<header class="header">
    <div class="container">
        <a href="#" class="header__toggle nav-toggle">
            <i>
                <span></span>
                <span></span>
                <span></span>
            </i>
            <span>меню</span>
        </a>
        <a href="/" class="header__logo">
            <img src="img/header__logo.png" class="img-fluid" alt="">
        </a>

        <div class="header__menu">

            <div class="nav-close nav-toggle"></div>

            <div class="header__menu_wrap">

                <ul class="header__nav">
                    <li>
                        <a href="#">каталог <i class="fa fa-chevron-down"></i></a>
                        <ul>
                            <li><a href="#">продукты</a></li>
                            <li>
                                <a href="#"><span>услуги <i class="fa fa-chevron-down"></i></span></a>
                                <ul>

                                    <li><a href="#">хранение, доставка, страхование</a></li>
                                    <li><a href="#">шеф-наладочные и монтажные работы</a></li>
                                    <li><a href="#">техническое обслуживание</a></li>
                                    <li><a href="#">сервис</a></li>
                                </ul>
                            </li>
                            <li><a href="#">наука</a></li>
                            <li><a href="#">работы</a></li>
                        </ul>
                    </li>
                    <li><a href="#">компания</a></li>
                    <li><a href="#">события</a></li>
                    <li><a href="#">контакты</a></li>
                </ul>

                <div class="header__search">
                    <div class="search">
                        <input type="text" class="search__input" placeholder="Поиск">
                        <button type="submit" class="search__button"><i class="fa fa-search"></i></button>
                    </div>
                </div>

                <ul class="header__lng">
                    <li class="active"><a href="#"><img src="img/icon-ru.png" class="img-fluid" alt=""></a></li>
                    <li><a href="#"><img src="img/icon-en.png" class="img-fluid" alt=""></a></li>
                </ul>

            </div>

        </div>

        <ul class="header__contact">
            <li>
                <a class="tel" href="tel:+74962198800">+7 <span>(496)</span> 219-88-00</a>
            </li>
            <li>
                <a href="#callback" class="btn btn-sm btn-modal">Обратный звонок</a>
            </li>
        </ul>

    </div>
</header>
