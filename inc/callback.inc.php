<div class="hide">
    <div class="modal" id="callback">
        <div class="modal__heading"><span>закажите обратный звонок</span></div>
        <form class="form">
            <div class="form-group">
                <input type="text" class="form-control" name="name" placeholder="Имя">
            </div>
            <div class="form-group">
                <input type="text" class="form-control" name="phone" placeholder="Телефон">
            </div>
            <div class="btn-group">
                <button type="submit" class="btn btn-md btn-blue">Отправить</button>
            </div>
        </form>
    </div>
</div>