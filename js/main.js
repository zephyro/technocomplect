
$(".btn-modal").fancybox({
    'padding'    : 0
});

$(function() {
    var pull = $('.nav-toggle');
    var menu = $('.page');

    $(pull).on('click', function(e) {
        e.preventDefault();
        menu.toggleClass('nav-open');
    });
});

var slider = new Swiper('.slider-content', {
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev'
    },
    loop: true,
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
        renderBullet: function (index, className) {
            return '<span class="' + className + '">0' + (index + 1) + '</span>';
        }
    }
});

var showcase = new Swiper('.showcase__slider', {
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev'
    },
    loop: true,
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
        renderBullet: function (index, className) {
            return '<span class="' + className + '">0' + (index + 1) + '</span>';
        }
    },
    slidesPerView: 6,
    spaceBetween: 30,

    breakpoints: {
        1230: {
            slidesPerView: 3,
            spaceBetween: 40
        },

        1024: {
            slidesPerView: 3,
            spaceBetween: 30
        },
        768: {
            slidesPerView: 1,
            spaceBetween: 10
        }
    }
});


jQuery(document).ready(function(){
    jQuery('.scroll-wrap').scrollbar();
    jQuery('.scroller').scrollbar();
});



$('.dealers__box_close').on('click touchstart', function(e){
    e.preventDefault();
    $('.dealers__wrap').removeClass('visible');
});

var thumb = new Swiper('.thumb-slider', {
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev'
    },
    direction: 'vertical',
    slidesPerView: 4,
    spaceBetween: 30,

    breakpoints: {
        1230: {
            slidesPerView: 4,
            spaceBetween: 30
        },

        1024: {
            slidesPerView: 4,
            spaceBetween: 20
        },
        768: {
            direction: 'horizontal',
            slidesPerView: 3,
            spaceBetween: 10
        }
    }
});


$('.accordion__toggle').on('click touchstart', function(e){
    e.preventDefault();
    $(this).closest('.accordion__item').toggleClass('active');
});

