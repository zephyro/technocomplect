<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <div class="heading">
                <div class="container">
                    <h1><span>Партнеры</span></h1>
                    <ul class="breadcrumb">
                        <li><a href="#">Главная</a></li>
                        <li>Партнеры</li>
                    </ul>
                </div>
            </div>

            <div class="main">
                <div class="subnav">
                    <div class="container">
                        <ul>
                            <li><a href="#">компания</a></li>
                            <li><a href="#">дилеры</a></li>
                            <li class="active"><a href="#">партнеры</a></li>
                            <li><a href="#">Сервис</a></li>
                        </ul>
                    </div>
                </div>
                <div class="content-gray content">
                    <div class="container">

                        <h2 class="text-center"><span>партнеры</span></h2>

                        <div class="partner">
                            <div class="partner__logo">
                                <img src="images/partner__logo1.png" class="img-fluid" alt="">
                            </div>
                            <div class="partner__info">
                                <div class="partner__name"><a href="#">АО «Ангстрем»</a></div>
                                <p>Производственная база «Ангстрем» включает в себя кристальное производство, сборочное производство и испытательный центр.</p>
                                <p>АО «Ангстрем» является единственным российским производителем электронной компонентной базы способным обеспечить серийный выпуск промышленного масштаба.</p>
                            </div>
                        </div>

                        <div class="partner">
                            <div class="partner__logo">
                                <img src="images/partner__logo2.png" class="img-fluid" alt="">
                            </div>
                            <div class="partner__info">
                                <div class="partner__name"><a href="#">OEZ</a></div>
                                <p>Чешская компания, разработчик и производитель низковольтного электротехнического оборудования, входит в концерн Siemens. Действует на рынке электротехники уже почти семьдесят лет.</p>
                            </div>
                        </div>

                        <div class="partner">
                            <div class="partner__logo">
                                <img src="images/partner__logo1.png" class="img-fluid" alt="">
                            </div>
                            <div class="partner__info">
                                <div class="partner__name"><a href="#">АО «Ангстрем»</a></div>
                                <p>Производственная база «Ангстрем» включает в себя кристальное производство, сборочное производство и испытательный центр.</p>
                                <p>АО «Ангстрем» является единственным российским производителем электронной компонентной базы способным обеспечить серийный выпуск промышленного масштаба.</p>
                            </div>
                        </div>

                        <div class="partner">
                            <div class="partner__logo">
                                <img src="images/partner__logo2.png" class="img-fluid" alt="">
                            </div>
                            <div class="partner__info">
                                <div class="partner__name"><a href="#">OEZ</a></div>
                                <p>Чешская компания, разработчик и производитель низковольтного электротехнического оборудования, входит в концерн Siemens. Действует на рынке электротехники уже почти семьдесят лет.</p>
                            </div>
                        </div>

                        <div class="partner">
                            <div class="partner__logo">
                                <img src="images/partner__logo1.png" class="img-fluid" alt="">
                            </div>
                            <div class="partner__info">
                                <div class="partner__name"><a href="#">АО «Ангстрем»</a></div>
                                <p>Производственная база «Ангстрем» включает в себя кристальное производство, сборочное производство и испытательный центр.</p>
                                <p>АО «Ангстрем» является единственным российским производителем электронной компонентной базы способным обеспечить серийный выпуск промышленного масштаба.</p>
                            </div>
                        </div>

                        <div class="partner">
                            <div class="partner__logo">
                                <img src="images/partner__logo2.png" class="img-fluid" alt="">
                            </div>
                            <div class="partner__info">
                                <div class="partner__name"><a href="#">OEZ</a></div>
                                <p>Чешская компания, разработчик и производитель низковольтного электротехнического оборудования, входит в концерн Siemens. Действует на рынке электротехники уже почти семьдесят лет.</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Callback -->
        <?php include('inc/callback.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
