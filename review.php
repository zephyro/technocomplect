<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <div class="heading">
                <div class="container">
                    <h1><span>Компания</span></h1>
                    <ul class="breadcrumb">
                        <li><a href="#">Главная</a></li>
                        <li>Отзывы</li>
                    </ul>
                </div>
            </div>

            <div class="main">
                <div class="subnav">
                    <div class="container">
                        <ul>
                            <li><a href="#">документы</a></li>
                            <li class="active"><a href="#">отзывы</a></li>
                            <li><a href="#">истории</a></li>
                            <li><a href="#">вакансии</a></li>
                        </ul>
                    </div>
                </div>
                <div class="content-gray content">
                    <div class="container">

                        <div class="text-content">

                            <h2 class="text-center"><span>Отзывы</span></h2>

                            <h3 class="fs-15 text-center">2018</h3>

                            <ul class="reviews">
                                <li>
                                    <a href="#">
                                        <div class="reviews__icon">
                                            <img src="img/icon-pdf.png" class="img-fluid" alt="">
                                        </div>
                                        <div class="reviews__text">
                                            <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</span>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="reviews__icon">
                                            <img src="img/icon-pdf.png" class="img-fluid" alt="">
                                        </div>
                                        <div class="reviews__text">
                                            <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</span>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="reviews__icon">
                                            <img src="img/icon-pdf.png" class="img-fluid" alt="">
                                        </div>
                                        <div class="reviews__text">
                                            <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</span>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="reviews__icon">
                                            <img src="img/icon-pdf.png" class="img-fluid" alt="">
                                        </div>
                                        <div class="reviews__text">
                                            <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</span>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="reviews__icon">
                                            <img src="img/icon-pdf.png" class="img-fluid" alt="">
                                        </div>
                                        <div class="reviews__text">
                                            <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</span>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="reviews__icon">
                                            <img src="img/icon-pdf.png" class="img-fluid" alt="">
                                        </div>
                                        <div class="reviews__text">
                                            <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</span>
                                        </div>
                                    </a>
                                </li>
                            </ul>

                            <h3 class="fs-15 text-center">2017</h3>

                            <ul class="reviews">
                                <li>
                                    <a href="#">
                                        <div class="reviews__icon">
                                            <img src="img/icon-pdf.png" class="img-fluid" alt="">
                                        </div>
                                        <div class="reviews__text">
                                            <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</span>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="reviews__icon">
                                            <img src="img/icon-pdf.png" class="img-fluid" alt="">
                                        </div>
                                        <div class="reviews__text">
                                            <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</span>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="reviews__icon">
                                            <img src="img/icon-pdf.png" class="img-fluid" alt="">
                                        </div>
                                        <div class="reviews__text">
                                            <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</span>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="reviews__icon">
                                            <img src="img/icon-pdf.png" class="img-fluid" alt="">
                                        </div>
                                        <div class="reviews__text">
                                            <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</span>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="reviews__icon">
                                            <img src="img/icon-pdf.png" class="img-fluid" alt="">
                                        </div>
                                        <div class="reviews__text">
                                            <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</span>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="reviews__icon">
                                            <img src="img/icon-pdf.png" class="img-fluid" alt="">
                                        </div>
                                        <div class="reviews__text">
                                            <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</span>
                                        </div>
                                    </a>
                                </li>
                            </ul>

                        </div>

                    </div>
                </div>
            </div>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Callback -->
        <?php include('inc/callback.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
