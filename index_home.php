<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <div class="slider">
                <div class="slider-content wiper-container">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="slider__item">
                                <div class="slider__inner">
                                    <img src="img/slide_01.jpg" class="img-fluid" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="slider__item">
                                <div class="slider__inner">
                                    <img src="img/slide_01.jpg" class="img-fluid" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="slider__item">
                                <div class="slider__inner">
                                    <img src="img/slide_01.jpg" class="img-fluid" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="slider__item">
                                <div class="slider__inner">
                                    <img src="img/slide_01.jpg" class="img-fluid" alt="">
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Add Arrows -->
                    <div class="swiper-button-next"></div>
                    <div class="swiper-button-prev"></div>

                    <!-- Add Pagination -->
                    <div class="swiper-pagination"></div>

                </div>
            </div>

            <section class="about">
                <div class="container">
                    <div class="about__row">
                        <div class="about__text">
                            <h1 class="text-left"><span>компания</span></h1>

                            <p>Основанная в 1996 году в г. Дубна ЗАО «МПОТК «ТЕХНОКОМПЛЕКТ» многие годы занимается разработкой, производством и поставкой электротехнического оборудования.</p>
                            <p>Сегодня на предприятии работает более 250 высокопрофессиональных специалистов, решающих самые сложные и нетривиальные задачи.</p>
                            <p>Разработка и широкое использование инновационных технологий в производственной деятельности компании обеспечили ее лидирующие позиции на рынке силовых полупроводниковых технологий, а независимый внутренний производственный цикл предопределил безусловные ценовые и качественные конкурентные преимущества производимой продукции.</p>
                            <br/>
                            <a href="#" class="btn btn-arrow-right">
                                <span>Подробнее</span>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </div>

                        <div class="about__image">
                            <div class="about__image_wrap">
                                <img src="images/about__img.jpg" class="img-fluid" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="showcase">
                <div class="container">
                    <h2><span>каталог</span></h2>
                    <div class="showcase__slider wiper-container">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <a href="#" class="goods">
                                    <div class="goods__image">
                                        <img src="images/product_01.jpg" class="img-fluid" alt="">
                                    </div>
                                    <h3>АУОТ-М «Дубна»</h3>
                                    <span>Зарядные устройства</span>
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="#" class="goods">
                                    <div class="goods__image">
                                        <img src="images/product_02.jpg" class="img-fluid" alt="">
                                    </div>
                                    <h3>АУОТ-М «Дубна»</h3>
                                    <span>Зарядные устройства</span>
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="#" class="goods">
                                    <div class="goods__image">
                                        <img src="images/product_03.jpg" class="img-fluid" alt="">
                                    </div>
                                    <h3>ШВР-М «Дубна»</h3>
                                    <span>Системы распределения переменного тока</span>
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="#" class="goods">
                                    <div class="goods__image">
                                        <img src="images/product_04.jpg" class="img-fluid" alt="">
                                    </div>
                                    <h3>Шкаф блока предохранителей</h3>
                                    <span>Контроль-измерительное оборудование</span>
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="#" class="goods">
                                    <div class="goods__image">
                                        <img src="images/product_05.jpg" class="img-fluid" alt="">
                                    </div>
                                    <h3>ПКИ-07 «Дубна»</h3>
                                    <span>Устройства гарантированного питания</span>
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="#" class="goods">
                                    <div class="goods__image">
                                        <img src="images/product_01.jpg" class="img-fluid" alt="">
                                    </div>
                                    <h3>АУОТ-М «Дубна»</h3>
                                    <span>Зарядные устройства</span>
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="#" class="goods">
                                    <div class="goods__image">
                                        <img src="images/product_02.jpg" class="img-fluid" alt="">
                                    </div>
                                    <h3>АУОТ-М «Дубна»</h3>
                                    <span>Зарядные устройства</span>
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="#" class="goods">
                                    <div class="goods__image">
                                        <img src="images/product_03.jpg" class="img-fluid" alt="">
                                    </div>
                                    <h3>ШВР-М «Дубна»</h3>
                                    <span>Системы распределения переменного тока</span>
                                </a>
                            </div>
                        </div>

                        <!-- Add Arrows -->
                        <div class="swiper-button-next"></div>
                        <div class="swiper-button-prev"></div>

                        <!-- Add Pagination -->
                        <div class="swiper-pagination"></div>

                    </div>
                </div>
            </section>

            <section class="videoBlock">
                <div class="container">
                    <div class="videoBlock__wrap">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe src="https://www.youtube.com/embed/gX2rDCYoLeo" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
            </section>

            <section class="eventsBlock">
                <div class="container">
                    <h2 class="text-center"><span>События</span></h2>
                    <ul class="events">
                        <li>
                            <span class="events__date">10.10.2018</span>
                            <a class="events__title" href="#">Поздравляем наших клиентов и коллег с Новым годом!</a>
                        </li>
                        <li>
                            <span class="events__date">10.10.2018</span>
                            <a class="events__title" href="#">Поздравляем наших клиентов и коллег с Новым годом!</a>
                        </li>
                        <li>
                            <span class="events__date">10.10.2018</span>
                            <a class="events__title" href="#">Поздравляем наших клиентов и коллег с Новым годом!</a>
                        </li>
                    </ul>
                    <div class="text-center">
                        <a href="#" class="btn btn-arrow-right">
                            <span>Все новости</span>
                            <i class="fa fa-angle-right"></i>
                        </a>
                    </div>
                </div>
            </section>

            <section class="activity">
                <div class="container">
                    <div class="activity__row">
                        <div class="activity__text">
                            <h2 class="text-left"><span>Направление деятельности</span></h2>
                            <uL>
                                <li>Разработка и производство современных систем постоянного оперативного тока и их элементов для объектов электроэнергетики</li>
                                <li>Научно-исследовательские и опытно-конструкторские изыскания в области силовых полупроводниковых технологий</li>
                                <li>Строительство и реконструкция объектов электросетевого хозяйства (выполнение строительно-монтажных и пусконаладочных работ)</li>
                                <li>Разработка и производство высоковольтных ячеек комплектных распределительных устройств 6,10 кВ</li>
                                <li>Проектирование вновь сооружаемых и реконструируемых объектов энергетики</li>
                                <li>Комплексное техническое комплектование (поставка полного спектра электротехнической продукции)</li>
                            </uL>
                        </div>
                        <div class="activity__image" style="background-image: url('images/activity__image_01.jpg');"></div>
                    </div>
                </div>
            </section>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Callback -->
        <?php include('inc/callback.inc.php') ?>
        <!-- -->

        <!-- Callback -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
