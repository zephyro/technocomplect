<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <div class="heading">
                <div class="container">
                    <h1><span>Заголовок H1</span></h1>
                    <ul class="breadcrumb">
                        <li><a href="#">Главная</a></li>
                        <li><a href="#">Системы постоянного тока</a></li>
                        <li>АОУТ-М «ДУБНА»</li>
                    </ul>
                </div>
            </div>

            <div class="main">

                <div class="content">
                    <div class="container">

                        <div class="product">

                            <div class="product__gallery">
                                <div class="gallery">
                                    <div class="gallery__image">
                                        <a href="images/product_item.jpg" data-fancybox="gallery">
                                            <img src="images/product_item.jpg" class="img-fluid" alt="">
                                        </a>
                                    </div>
                                    <div class="gallery__thumbs">
                                        <div class="thumb-slider swiper-container">
                                            <div class="swiper-wrapper">
                                                <div class="swiper-slide">
                                                    <a class="gallery__thumbs_item" href="#">
                                                        <img src="images/product_item.jpg" class="img-fluid" alt="">
                                                        <span class="loop"><i class="fa fa-search-plus"></i></span>
                                                    </a>
                                                </div>
                                                <div class="swiper-slide">
                                                    <a class="gallery__thumbs_item" href="#">
                                                        <img src="images/product_item.jpg" class="img-fluid" alt="">
                                                        <span class="loop"><i class="fa fa-search-plus"></i></span>
                                                    </a>
                                                </div>
                                                <div class="swiper-slide">
                                                    <a class="gallery__thumbs_item" href="#">
                                                        <img src="images/product_item.jpg" class="img-fluid" alt="">
                                                        <span class="loop"><i class="fa fa-search-plus"></i></span>
                                                    </a>
                                                </div>
                                                <div class="swiper-slide">
                                                    <a class="gallery__thumbs_item" href="#">
                                                        <img src="images/product_item.jpg" class="img-fluid" alt="">
                                                        <span class="loop"><i class="fa fa-search-plus"></i></span>
                                                    </a>
                                                </div>
                                                <div class="swiper-slide">
                                                    <a class="gallery__thumbs_item" href="#">
                                                        <img src="images/product_item.jpg" class="img-fluid" alt="">
                                                        <span class="loop"><i class="fa fa-search-plus"></i></span>
                                                    </a>
                                                </div>
                                                <div class="swiper-slide">
                                                    <a class="gallery__thumbs_item" href="#">
                                                        <img src="images/product_item.jpg" class="img-fluid" alt="">
                                                        <span class="loop"><i class="fa fa-search-plus"></i></span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Add Arrows -->
                                        <div class="swiper-button-next"></div>
                                        <div class="swiper-button-prev"></div>

                                    </div>
                                </div>
                                <span class="goods__tag special">спецпредложение</span>
                            </div>

                            <div class="product__info">
                                <h3 class="fs-15">назначение</h3>
                                <p>Аппараты управления оперативным током АУОТ-М«Дубна» предназначены для:</p>
                                <ul>

                                    <li>надежного питания электроприемников постоянного тока;</li>
                                    <li>питания цепей защиты, автоматики и телемеханики, аппаратуры дистанционного управления, аварийной и предупредительной сигнализации;</li>
                                    <li>заряда и подзаряда обслуживаемых и необслуживаемых свинцово-кислотных аккумуляторных батарей (АБ) различных типов и технологий изготовления (далее АБ), подключаемых отдельно или в буферном режиме с нагрузкой;</li>
                                    <li>включения привода высоковольтного выключателя.</li>
                                </ul>
                                <h3>область применения</h3>
                                <p>Системы оперативного постоянного тока (СОПТ) на энергообъектах при использовании в качестве зарядновы прямительных устройств (ЗВУ), а также иные системы, где необходимо стабилизированное постоянное напряжение (ток) для питания электроустановок.</p>
                            </div>

                            <div class="product__text">
                                <h3 class="fs-15">работа устройства</h3>
                                <p>Изделие имеет два ввода питания переменного тока (основной и резервный), коммутируемых на преобразовательную часть устройства с помощью блока АВР. Преобразовательная часть включает в себя два взаиморезервируемых преобразовательных блока. Каждый преобразовательный блок состоит из входного трехфазного выпрямителя, сглаживающего фильтра, инвертора, высокочастотного трансформатора, выходного выпрямителя и LC-фильтра. Оба блока находятся под синхронным управлением единой системы управления, которая в зависимости от заданных пользователем режимов либо включает в работу один из блоков, либо обеспечивает их совместное включение на общий выход. Высокочастотные трансформаторы в составе преобразователя обеспечивают гальваническую развязку между вводами питания переменного тока и выходом агрегата.</p>
                                <p>При одновременной работе преобразовательных блоков обеспечивается автоматический выбор и переключение на режим с большим приоритетом:</p>
                                <ul>
                                    <li>При запрете одновременной работы преобразовательных блоков, в зависимости от наличия напряжений основной и резервной питающих сетей и исправности преобразовать</li>
                                    <li>питание преобразовательных блоков от резервной питающей сети.</li>
                                </ul>
                                <p> При запрете одновременной работы преобразовательных блоков, в зависимости от наличия напряжений основной и резервной питающих сетей и исправности преобразовательных блоков, обеспечивается автоматический выбор и переключение на режим с наибольшим приоритетом:
                                </p>
                               <ul>
                                    <li>При запрете одновременной работы преобразовательных блоков, в зависимости от наличия напряжений основной и резервной питающих сетей и исправности преобразовать</li>
                                    <li>питание преобразовательных блоков от резервной питающей сети.</li>
                                </ul>

                                <p> Инвертор выполнен на основе IGBT транзисторов, коммутирующих напряжение с частотой около 10 кГц. Нагрузкой инвертора является первичная обмотка трансформатора. Напряжение с вторичной обмотки выпрямляется диодами выходного ВЧ-выпрямителя и через выходной LC-фильтр поступает на выход устройства. Общая система управления, построенная на высокопроизводительном микроконтроллере, обеспечивает контроль, правление и различные защиты, как самого агрегата, так и буферно подключенной АБ. Для контроля и мониторинга параметров АБ АУОТ-М«Дубна» комплектуется датчиком тока, устанавливаемом в цепи АБ, а также датчиком температуры АБ – для обеспечения термокомпенсации напряжения подзаряда. Помимо технологических функций система управления обеспечивает интерфейсные функции для встраивания агрегата в системы АСУ ТП. Человеко-машинный интерфейс организован на базе графического дисплея с пленочной клавиатурой.</p>
                            </div>

                            <div class="product__data">

                                <div class="product__pic">
                                    <div class="image">
                                        <a href="images/data_01.jpg" data-fancybox="data">
                                            <img src="images/data_01.jpg" class="img-fluid" alt="">
                                        </a>
                                    </div>
                                    <div class="image">
                                        <a href="images/data_02.jpg" data-fancybox="data">
                                            <img src="images/data_02.jpg" class="img-fluid" alt="">
                                        </a>
                                    </div>
                                </div>
                                <div class="product__cert">
                                    <div class="product__cert_item">
                                        <a href="images/cert_01.jpg" data-fancybox="data">
                                            <img src="images/cert_01.jpg" class="img-fluid" alt="">
                                        </a>
                                    </div>
                                    <div class="product__cert_item">
                                        <a href="images/cert_02.jpg" data-fancybox="data">
                                            <img src="images/cert_02.jpg" class="img-fluid" alt="">
                                        </a>
                                    </div>
                                    <div class="product__cert_item">
                                        <a href="images/cert_03.jpg" data-fancybox="data">
                                            <img src="images/cert_03.jpg" class="img-fluid" alt="">
                                        </a>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>

                <div class="content-gray content">
                    <div class="container container-mobile">
                        <h3 class="fs-15 mb-10">технические характеристики</h3>
                        <p class="text-center"><strong>1.</strong> Технические параметры силовой части по исполнениям</p>

                        <div class="table-responsive">
                            <table class="table">
                                <tr>
                                    <th>Питающая сеть</th>
                                    <th>Номинальный выходной ток при одновременной работе преобразовательных блоков*), [А]</th>
                                    <th>Диапазон регулирования выходного напряжения [B]</th>
                                    <th>Номинальное выходное напряжение [B]</th>
                                </tr>
                                <tr>
                                    <td rowspan="3">3х380 В, <br/>45 ÷ 55 Гц</td>
                                    <td>50 (80*)</td>
                                    <td>85 - 140</td>
                                    <td>115</td>
                                </tr>
                                <tr>
                                    <td>50 (80*)</td>
                                    <td>85 - 140</td>
                                    <td>115</td>
                                </tr>
                            </table>
                            <br/>
                            <br/>
                        </div>
                        <p class="text-center"><strong>2.</strong> Общие технические характеристики</p>
                        <div class="table-responsive">
                            <table class="table">
                                <tr>
                                    <th>Наименование</th>
                                    <th>Значение</th>
                                </tr>
                                <tr>
                                    <td colspan="2" class="table-subtitle">входные параметры</td>
                                </tr>
                                <tr>
                                    <td>Тип входного соединения</td>
                                    <td>3L+P</td>
                                </tr>
                                <tr>
                                    <td>
                                        Напряжение питающей сети, В, для кодов:
                                        <br/>
                                        2
                                        <br/>
                                        3
                                    </td>
                                    <td>
                                        <br/>
                                        380
                                        <br/>
                                        220
                                    </td>
                                </tr>
                                <tr>
                                    <td>Допускаемые отклонения напряжения питающей сети, %, не более</td>
                                    <td>от -15 до +10</td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="table-subtitle">выходные параметры</td>
                                </tr>
                                <tr>
                                    <td>Коэффициент пульсации выходного напряжения в режимах заряда, подзаряда стабилизированным напряжением, %, не более</td>
                                    <td>0,2</td>
                                </tr>
                                <tr>
                                    <td>
                                        Номинальный выходнок ток, А, для кодов:
                                        <br/>
                                        2
                                        <br/>
                                        3
                                    </td>
                                    <td>
                                        <br/>
                                        25, 50, 100
                                        <br/>
                                        16, 25, 50, 100
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="content">
                    <div class="container">
                        <div class="text-content">
                            <h3 class="fs-15">ФУНКЦИОНАЛЬНЫЕ ОСОБЕННОСТ</h3>
                            <p>Электропитание изделий от основной и резервной сетей трёхфазного переменного тока с действием АВР;</p>
                            <ul>
                                <li>Преобразование напряжения сети переменного тока в напряжение постоянного тока осуществляется при помощи двух взаимно резервируемых независимых преобразователей на базе IGBT-транзисторов, с возможностью объединения мощности;</li>
                                <li>Охлаждение преобразовательных блоков – воздушное, принудительное с пропорциональным управлением вентиляторами;</li>
                                <li>Возможность дистанционного управление внешними сигналами типа «сухой контакт» с действиями
                                    <ul>
                                        <li>— приостановка работы;</li>
                                        <li>— приостановка заряда аккумуляторной батареи.</li>
                                    </ul>
                                </li>
                                <li>Ускоренный заряд аккумуляторной батареи методами U, IU, IUI;</li>
                                <li>Выравнивающий заряд АБ;</li>
                            </ul>
                            <br/>
                            <ul class="documents">
                                <li>
                                    <a href="#" class="documents__item">
                                        <div class="documents__icon">
                                            <img src="img/icon-pdf.png" class="img-fluid" alt="">
                                        </div>
                                        <div class="documents__text"><span><span>ШЖИС.435311.005 РЭ АУОТ-М</span></span></div>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="documents__item">
                                        <div class="documents__icon">
                                            <img src="img/icon-word.png" class="img-fluid" alt="">
                                        </div>
                                        <div class="documents__text"><span><span>ОЛ на АУОТ 50 и 100</span></span></div>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="content-gray content">
                    <div class="container">
                        <div class="form-title"><span>Оставить заявку на покупку Ауот-М «Дубна»</span></div>
                        <form class="form">
                            <ul class="order">
                                <li><input type="text" class="form-control form-gray" name="name" placeholder="Имя"></li>
                                <li><input type="text" class="form-control form-gray" name="email" placeholder="Email"></li>
                                <li><input type="text" class="form-control form-gray" name="phone" placeholder="Телефон"></li>
                                <li><button class="btn btn-md btn-blue">Отправить</button></li>
                            </ul>
                        </form>
                    </div>
                </div>

            </div>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Callback -->
        <?php include('inc/callback.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
