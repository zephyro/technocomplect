<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <div class="heading">
                <div class="container">
                    <h1><span>ошибка 404</span></h1>
                    <ul class="breadcrumb">
                        <li><a href="#">Главная</a></li>
                        <li>404</li>
                    </ul>
                </div>
            </div>

            <div class="main">
                <div class="container">
                    <div class="page404 text-center">
                        <p>Страница по введеному адресу не существует или удалена администратором.</p>
                        <br/>
                        <a href="#" class="btn">На главную</a>
                    </div>
                </div>
            </div>


            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Callback -->
        <?php include('inc/callback.inc.php') ?>
        <!-- -->

        <!-- Callback -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
