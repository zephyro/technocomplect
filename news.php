<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <div class="heading">
                <div class="container">
                    <h1><span>События</span></h1>
                    <ul class="breadcrumb">
                        <li><a href="#">Главная</a></li>
                        <li>События</li>
                    </ul>
                </div>
            </div>

            <div class="main">
                <div class="subnav">
                    <div class="container">
                        <ul>
                            <li class="active"><a href="#">новости</a></li>
                            <li><a href="#">публикации</a></li>
                            <li><a href="#">выставки</a></li>
                            <li><a href="#">совещания</a></li>
                        </ul>
                    </div>
                </div>
                <div class="content-gray content">
                    <div class="container">

                        <div class="text-content">

                            <h2 class="text-center"><span>новости</span></h2>

                            <div class="text-content">

                                <div class="news-row">

                                    <article class="news">
                                        <div class="news__image">
                                            <a href="#"></a>
                                        </div>
                                        <div class="news__intro">
                                            <div class="news__inner">
                                                <div class="news__date">10.10.2018</div>
                                                <a href="#">ЗАО «МПОТК «ТЕХНОКОМПЛЕКТ» произвело поставку для нужд Министерства обороны Российской Федерации</a>
                                            </div>
                                        </div>
                                    </article>

                                    <article class="news">
                                        <div class="news__image">
                                            <a href="#"></a>
                                        </div>
                                        <div class="news__intro">
                                            <div class="news__inner">
                                                <div class="news__date">10.10.2018</div>
                                                <a href="#">Эксклюзивные права на реализацию выпускаемого оборудования ЗАО «Конвертор» г.Саранск</a>
                                            </div>
                                        </div>
                                    </article>

                                    <article class="news">
                                        <div class="news__image">
                                            <a href="#">
                                                <img src="images/news_01.png" class="img-fluid" alt="">
                                            </a>
                                        </div>
                                        <div class="news__intro">
                                            <div class="news__inner">
                                                <div class="news__date">10.10.2018</div>
                                                <a href="#">Поздравляем наших клиентов и коллег с Новым годом!</a>
                                            </div>
                                        </div>
                                    </article>

                                    <article class="news">
                                        <div class="news__image">
                                            <a href="#">
                                                <img src="images/news__02.png" class="img-fluid" alt="">
                                            </a>
                                        </div>
                                        <div class="news__intro">
                                            <div class="news__inner">
                                                <div class="news__date">10.10.2018</div>
                                                <a href="#">Поздравляем с Днем Энергетика!</a>
                                            </div>
                                        </div>
                                    </article>

                                    <article class="news">
                                        <div class="news__image">
                                            <a href="#">
                                                <img src="images/news_01.png" class="img-fluid" alt="">
                                            </a>
                                        </div>
                                        <div class="news__intro">
                                            <div class="news__inner">
                                                <div class="news__date">10.10.2018</div>
                                                <a href="#">Поздравляем наших клиентов и коллег с Новым годом!</a>
                                            </div>
                                        </div>
                                    </article>

                                    <article class="news">
                                        <div class="news__image">
                                            <a href="#">
                                                <img src="images/news__02.png" class="img-fluid" alt="">
                                            </a>
                                        </div>
                                        <div class="news__intro">
                                            <div class="news__inner">
                                                <div class="news__date">10.10.2018</div>
                                                <a href="#">Поздравляем с Днем Энергетика!</a>
                                            </div>
                                        </div>
                                    </article>

                                </div>

                                <ul class="pagination">
                                    <li><a href="#"><i class="fa fa-angle-left"></i></a></li>
                                    <li class="active"><a href="#">01</a></li>
                                    <li><a href="#">02</a></li>
                                    <li><a href="#">03</a></li>
                                    <li><a href="#">04</a></li>
                                    <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                                </ul>

                            </div>

                        </div>

                    </div>
                </div>
            </div>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Callback -->
        <?php include('inc/callback.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
